import pandas
import time
def generartabla(reg,paises,idiomas):
    tabla=pandas.DataFrame(columns=['Region', 'Country', 'Language'])
    tiempos=[]
    for i in range(len(reg)):
        start = time.time()
        tabla.loc[i]=[ reg[i], paises[i], idiomas[i]]
        tiempoestimado = round((time.time() - start)*1000,2)
        tiempos.append(tiempoestimado)
    tabla['Time (ms)']=tiempos
    return tabla,tiempos

def mostrartiempos(tabla):
    total=tabla['Time (ms)'].sum()
    minimo=tabla['Time (ms)'].min()
    maximo=tabla['Time (ms)'].max()
    promedio=tabla['Time (ms)'].mean()
    texto=" Tiempo total: "+str(total)+"\n Tiempo mínimo: "+str(minimo)+"\n Tiempo máximo: "+str(maximo)+"\n Tiempo promedio: "+str(promedio)
    return texto