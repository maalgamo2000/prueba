import sqlite3
import json
from sqlite3 import Error

def generarconexion(archivodb="Persistencia/basedatos.bd"):

    conexion = None
    try:
        conexion = sqlite3.connect(archivodb)
       # print("conexión establecida")
    except Error as e:
        print(e)
    return conexion

def generartabladb(conexion):
    sentencia="CREATE TABLE paises(id INTEGER PRIMARY KEY AUTOINCREMENT,Region text, Country text, Language text, Time float)"
    try:
        cursor = conexion.cursor()
        cursor.execute(sentencia)
        conexion.commit()
    except Error as e:
        print(e)

def insertar(conexion,datos):
    try:
        cursor = conexion.cursor()
        cursor.execute("INSERT INTO paises(Region, Country, Language, Time) VALUES(?, ?, ?, ?)", datos)
        conexion.commit()
    except Error as e:
        print(e)

def ins(conexion,reg,paises,idiomas,tiempos):
    for i in range(len(reg)):
        datos = (reg[i], paises[i], idiomas[i], tiempos[i])
        insertar(conexion,datos)

def guardarJson(conexion):

    try:
        cursor = conexion.cursor()
        cursor.execute("SELECT * FROM paises")
        conexion.commit()
        resultado = cursor.fetchall()
        with open('data.json', 'w') as file:
            json.dump(resultado, file, indent=4)
    
    except Error as e:
        print(e)
    