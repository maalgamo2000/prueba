import requests
import json
import random
import hashlib

class Informacion:
    url=''
    headers=''
    def __init__(self,url='',header=''):
        self.url=url
        self.headers=header

    def traerregiones(self):
        self.url = "https://rapidapi.p.rapidapi.com/all"

        self.headers = {
        'x-rapidapi-host': "restcountries-v1.p.rapidapi.com",
        'x-rapidapi-key': "60a52dbfb8msh608f0279147bdadp11142djsn0220b4df9166"
            }
        response = requests.request("GET", self.url, headers=self.headers)
        res = json.loads(response.text)
        regiones=[]
        for i in range(len(res)):
            if(res[i]['region'] not in regiones):
                regiones.append(res[i]['region'])
                    
        return regiones
    
    def traerpaises(self,regiones):
        self.url = "https://restcountries.eu/rest/v2/all"
        response = requests.request("GET", self.url)
        countries= json.loads(response.text)
        paises=[]
        reg=[]
        vals=[]
        cont=0
        while(cont<7):
            val=random.randint(0, len(countries)-1)
            pais=countries[val]['region']
            if(pais in regiones):
                cont=cont+1
                vals.append(val)
                reg.append(pais)
                regiones.remove(pais)
                paises.append(countries[val]['name'])
        return reg,paises,vals,countries
    
    def traeridioma(self,countries,vals):
        idiomas=[]
        for i in range(len(vals)):
            lang=countries[vals[i]]['languages'][0]['name']
            lang=hashlib.sha1(lang.encode())
            idiomas.append(lang.hexdigest())
        return idiomas

class Paises:

    regiones=[]
    paises=[]
    idiomas=[]
    tiempos=[]
    inf=Informacion()

    def generardatos(self):

        reg=self.inf.traerregiones()
        res=self.inf.traerpaises(regiones=reg)
        self.regiones=res[0]
        self.paises=res[1]
        self.idiomas=self.inf.traeridioma(countries=res[3],vals=res[2])
